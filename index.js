let hamburgerMenu = document.getElementById("hamburgerMenu");
let closeIcon = document.getElementById("closeIcon");
let navItems = document.getElementById("navItems");
let modelContent = document.getElementById("modelContent");

hamburgerMenu.addEventListener("click", (event) => {
    hamburgerMenu.classList.add("mockupDisplay");
    closeIcon.classList.remove("closeicon");
    modelContent.classList.remove("mockupDisplay");
    navItems.classList.remove("mockupDisplay");
})

closeIcon.addEventListener("click", () => {
    hamburgerMenu.classList.remove("mockupDisplay");
    closeIcon.classList.add("closeicon");
    navItems.classList.add("mockupDisplay");
    modelContent.classList.add("mockupDisplay");
})